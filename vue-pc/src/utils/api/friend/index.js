import http from "@/utils/requestConfig.js";

export default {
  //接受好友
  acceptFriend: function(param) {
    return http.post("/app/friend/accept", param);
  },
  //拒绝好友
  refuseFriend: function(param) {
    return http.post("/app/friend/refuse", param);
  },
  //添加好友
  addFriend: function(param) {
    return http.post("/app/friend/addAsk", param);
  },

  //新的好友
  findFriend: function(param) {
    return http.post("/app/friend/findAsk", param);
  },

  //好友消息
  friendMsg: function(param) {
    return http.post("/app/friend/findMsg", param);
  },

  //我的通讯录
  friendList: function(param) {
    return http.post("/app/friend/list", param);
  },

  //查询好友
  friendQuery: function(param) {
    return http.post("/app/friend/query", param);
  },
  //我的群聊
  groupList: function(param) {
    return http.post("/app/group/list", param);
  }
};
