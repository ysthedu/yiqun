import http from "@/utils/requestConfig.js";

// 创建红包(发私包)
function createPrivatePacket(params) {
  return http.post("/app/packet/createPrivatePacket", params);
}

// 抢私包
function robPrivatePacket(params) {
  return http.post("/app/packet/robPrivatePacket", params);
}

// 创建群红包
function createPacket(params) {
  return http.post("/app/packet/createPacket", params);
}

// 抢群红包
function robPacket(params) {
  return http.post("/app/packet/robPacket", params);
}

export default {
  createPrivatePacket,
  robPrivatePacket,
  createPacket,
  robPacket
};
