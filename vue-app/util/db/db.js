import cache from '@/util/cache.js'
import store from '@/store/index.js'
var chatfix = 'list_chat_';
var Msgfix = 'list_msg_';

export const putGroupMsg = (a) => {
	var c = []
	for(var i in a){
		if(a[i].msgList.length>0){
			c.push(a[i].msgList[a[i].msgList.length-1]);
		}
		
		let msgList = a[i].msgList;
		cache.set(Msgfix + store.state.user.operId + '_' + a[i].groupId, msgList)
	}
}


export const putFriendMsg = (a,b) =>{
	let msgList = b.list
	cache.set(Msgfix+ store.state.user.operId + '_' + a, msgList)
}


export const getMsgList = (chatObj) => {
	return new Promise((resolve, reject) => {
		let msgList = cache.get(Msgfix + store.state.user.operId + '_' + chatObj.chatId)
		resolve(msgList)
	});
}

export const removeMsgList = (chat) => {
	let msgListName = cache.get(Msgfix + store.state.user.operId + '_' + chat.chatId)
	cache.remove(msgListName)
}

export const openChat = (chat) => {
	return new Promise((resolve, reject) => {
		const cacheChatName = chatfix + store.state.user.operId
		let chatList = cache.get(cacheChatName);
		if (chatList) {
			for (var i in chatList) {
				if (chatList[i].chatId == chat.chatId) {
					reduceUserUnreadNumberTotal(chatList[i])
					chatList[i].unreadNumber = 0
					chatList[i].lastOpenTime = new Date()
					chatList[i].isOpen = true
					chatList[i].weakPrompt = false // 弱提醒
					continue
				}
			}
			cache.set(cacheChatName, chatList)
			store.commit("setConversation", chatList)
		}
	});
}

export const closeChat = (chat) => {
	return new Promise((resolve, reject) => {
		const cacheChatName = chatfix + store.state.user.operId
		let chatList = cache.get(cacheChatName);
		if (chatList) {
			for (var i in chatList) {
				if (chatList[i].chatId == chat.chatId) {
					chatList[i].unreadNumber = 0
					chatList[i].isOpen = false
					chatList[i].lastCloseTime = new Date()
					continue
				}
			}
			cache.set(cacheChatName, chatList)
			store.commit("setConversation", chatList)
		}
	});
}


function addMsg(chat) {
	let msgListCacheName = Msgfix + store.state.user.operId + '_' + chat.chatId;
	if(chat.chatType == 0){
		console.log(JSON.stringify(chat))
		if(chat.fromUserId == store.state.user.operId){
			msgListCacheName = Msgfix + store.state.user.operId + '_' + chat.toUserId;
		} else {
			msgListCacheName = Msgfix + store.state.user.operId + '_' + chat.fromUserId;
		}
	}
	
	let MsgList = cache.get(msgListCacheName)
	if (!MsgList) {
		let MsgList2 = [];
		MsgList2.push(chat);
		cache.set(msgListCacheName, MsgList2);
		return;
	}

	let hasMsg = false
	for (let i in MsgList) {
		if (MsgList[i].hasBeenSentId == chat.hasBeenSentId) {
		  hasMsg = true
		}
	}
	
	
	if(!hasMsg){
		if (MsgList.length >= 10) {
			MsgList.splice(0, 1);
		}
		MsgList.push(chat)
		hasMsg = false
		cache.set(msgListCacheName, MsgList)
	}
}


export const modifyPacket = (chat) => {
	let msgListCacheName = Msgfix + store.state.user.operId + '_' + chat.chatId;
	let msglist = cache.get(msgListCacheName);
	if (!msglist) {
		let item = []
		item.push(chat)
		cache.set(msgListCacheName, item)
		return;
	}
	for (var i in msglist) {
		if (msglist[i].hasBeenSentId == chat.hasBeenSentId) {
			msglist[i].content = chat.content
		}
	}
	cache.set(msgListCacheName, msglist)
}

export const modifyChat = (chat) => {
	const cacheChatListName = chatfix + store.state.user.operId
	let chatList = cache.get(cacheChatListName)
	if (!chatList) return;
	for (var i in chatList) {
		if (chatList[i].chatId == chat.chatId) {
			chatList[i] = chat
		}
	}
	cache.set(cacheChatListName, chatList)
}

export const removeChat = (chat) => {
	const cacheChatListName = chatfix + store.state.user.operId
	let chatList = cache.get(cacheChatListName)
	if (!chatList) return;
	let idx = -1;
	for (var i in chatList) {
		if (chatList[i].chatId == chat.chatId) {
			idx = i
			break
		}
	}
	if (idx != -1) chatList.splice(idx, 1);
	cache.set(cacheChatListName, chatList)
}

export const addFriendType = (chat) =>{
	const cacheChatName = chatfix + store.state.user.operId
	let chatList = cache.get(cacheChatName)
	if (!chatList || chatList.length <= 0) {
		chatList = [];
		chatList.push(chat);
	}
	
	let isHas = false
	
	if(chat.fromUserId != store.state.user.operId){
		if(chat.toUserId == store.state.user.operId){
			chat.chatId = chat.fromUserId
			chat.chatName = chat.fromUserName
			chat.avatar  = chat.fromUserHeadImg
			chat.lastOpenTime = Date.now()
			chat.unreadNumber = 0
			
			
			for(var i in chatList){
				if(chatList[i].chatId == chat.chatId){
					chatList[i].content =  chat.content
					chatList[i].unreadNumber = chatList[i].unreadNumber+1
					chatList[i].lastOpenTime = Date.now()
					isHas = true
				}
			}
			
		}
	} else {
		chat.chatId = chat.toUserId
		chat.chatName = chat.toUserName
		chat.avatar  = chat.toUserHeadImg
		chat.lastOpenTime = Date.now()
		chat.unreadNumber = 0
		
		for(var i in chatList){
			if(chatList[i].chatId == chat.chatId){
				chatList[i].content =  chat.content
				chatList[i].lastOpenTime = Date.now()
				isHas = true
			}
		}
		
	}
	
	if(!isHas){
		chatList.push(chat)
		isHas = false
	}
	store.commit("setConversation", chatList)
	cache.set(cacheChatName, chatList)
}

export const addChatAll = (chatList) =>{
	const cacheChatName = chatfix + store.state.user.operId
	cache.set(cacheChatName, chatList)
}


export const addGroupType = (chat) =>{
	const cacheChatName = chatfix + store.state.user.operId
	chat.isItMe = chat.fromUserId == store.state.user.operId
	chat.lastOpenTime = Date.now()
	chat.chatId = chat.toUserId
	chat.chatName = chat.toUserName
	chat.avatar = chat.toUserHeadImg
	chat.unreadNumber = 0
	
	let chatList = cache.get(cacheChatName)
	if (!chatList || chatList.length <= 0) {
		chatList = [];
		chatList.push(chat);
	} else {
		let isSameChat = false
		for (var i in chatList) {
			if (chatList[i].chatId == chat.chatId) {
				chatList[i].content =  chat.content
				chatList[i].unreadNumber = chatList[i].unreadNumber+1
				chatList[i].lastOpenTime = Date.now()
				isSameChat = true
			}
		}
		if(!isSameChat){
			chatList.push(chat);
			isSameChat = false
		}
	}

	
	cache.set(cacheChatName, chatList)
	store.commit("setConversation", chatList)
	
	if (chat.contentType == 5) {
		modifyPacket(chat)
		return
	}
}

export const addChat = (chat) => {
	return new Promise((resolve, reject) => {
		if (chat.command == 3 || chat.command == 4) {
			if (chat.content == "") {
				return;
			}
			if(chat.chatType == 1){
				addGroupType(chat)
			}
			if(chat.chatType == 0){
				addFriendType(chat)
			}
			const cacheChatName = chatfix + store.state.user.operId
			let chatList = cache.get(cacheChatName);
			if (chatList) {
				chatList = chatList.sort(function(a, b){return a.lastOpenTime-b.lastOpenTime});
				store.commit("setConversation", chatList)
			}
			
			addMsg(chat)
		}
	});
}

/**
 * 增加当前用户总的未读数量
 */
export const increaseUserUnreadNumberTotal = (chat) => {
	const user = cache.get("user")
	user.totalUnreadNumber += 1
	cache.set("user", user)
	store.commit("setUser", user);
}

/**
 * 减少当前用户未读总数
 * @param {object} caht 
 */
export const reduceUserUnreadNumberTotal = (chat) => {
	const user = cache.get("user")
	user.totalUnreadNumber -= chat.unreadNumber
	cache.set("user", user)
	store.commit("setUser", user);
}
