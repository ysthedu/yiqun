import http from '@/util/requestConfig.js';

export default {
	setGroupRole: function(param) {
		return http.post('/app/group/user/role', param);
	}
}
