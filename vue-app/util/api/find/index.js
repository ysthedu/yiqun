import http from '@/util/requestConfig.js';

export default {
	//发现列表
	findList: function(param) {
		return http.post('/app/post/list', param);
	},
	//添加评论
	addFind: function(param) {
		return http.post('/app/comment/add', param);
	},
	//发布朋友圈
	releaseFriend: function(param) {
		return http.post('/app/post/add', param)
	},
	//点赞和取消点赞
	fabulousAddOrDelete: function(param) {
		return http.post(param.url, {
			postId: param.postId
		})
	},
	//点赞
	fabulousAdd: function(param) {
		return http.post('/app/fabulous/add', param)
	},
	//取消点赞
	fabulousDelete: function(param) {
		return http.post('/app/fabulous/delete', param)
	},

}
