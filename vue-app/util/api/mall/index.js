import http from '@/util/requestConfig.js';

export default {
	shopList: function(param) {
		return http.post('/app/shop/list', param);
	},
	shopGet: function(param) {
		return http.post('/app/shop/get', param);
	}
}
