//扫一扫

import cache from '@/utils/cache'
import friend from '@/api/friend'
import chat from '@/api/chat'

export const scan = () => {
	uni.scanCode({
		onlyFromCamera: false,
		scanType: "qrCode",
		success: function(e) {
			console.log("调用扫码成功数据", e)
			const user = cache.get('user')
			const result = JSON.parse(decodeURIComponent(e.result))
			console.log("二维码内容==", result)
			if (!result.type) {
				errQrCode(result)
				return false;
			}
			//1:加好友,2:加群
			switch (result.type) {
				case 1:
					addUser(result.value, user.operId)
					break;
				case 2:
					addGroupUser(result.value, user.operId, user.nickName)
					break;
				default:
					errQrCode(result)
					break;
			}


			// if (result.type == 1) {
			// 	addUser(result.value, user.operId)
			// }
			// //1:加好友,2:加群
			// if (result.type == 2) {
			// 	addGroupUser(result.value, user.operId, user.nickName)
			// }
		},
		fail: function(e) {
			const result = JSON.parse(decodeURIComponent(e.result))
			errQrCode(result)
		},
		complete: function(e) {
			console.log("扫码结束", e)
		}
	})
}

/**
 * 扫码失败
 */
export const errQrCode = (result) => {
	uni.showModal({
		title: "扫码失败",
		content: JSON.stringify(result)
	})
}

/**
 * * 添加好友
 * @param {*} friendId 
 * @param {*} userId 
 */
export const addUser = (friendId, userId) => {
	uni.showModal({
		content: "是否申请添加对方好友",
		success: function(e) {
			if (e.confirm) {
				friend.addFriend({
					userId: userId,
					friendId: friendId
				}).then(res => {
					console.log("扫码加好友===", res)
					let content = '申请添加好友失败,请稍后重试'
					if (res.success === true) {
						content = '申请添加好友成功'
					}
					uni.showModal({
						content: content,
						showCancel: false
					})
				})
			}
		}
	})
}

/**
 * 扫码入群
 * @param {*} groupId 
 * @param {*} userId 
 * @param {*} nickName 
 */
export const addGroupUser = (groupId, userId, nickName) => {
	uni.showModal({
		content: "是否要加入该群",
		success: function(e) {
			if (e.confirm) {
				const param = {
					groupId: groupId,
					userIds: [userId],
					userNames: [nickName]
				}
				chat.groupUserAdd(param).then(res => {
					console.log("扫码加群后端返回的数据", res)
					let title = "加群成功"
					if (true !== res.success) {
						title = "加群失败,请稍后再试"
					}
					uni.showToast({
						title: title,
						icon: "none"
					})
				})
			}
		}
	})
}
