import http from '@/util/requestConfig.js';

export default {
	updatePassword: function(param) {
		return http.post('/app/user/updatePassword', param);
	},
	register: function(param){
		return http.post('/register/register', param);
	},
	sendSms: function(param){
		return http.post('/register/sendSms', param);
	},
	getUser: function(param){
		return http.post('/app/user/getUser', param);
	},
};
