import http from '@/util/requestConfig.js';

export default {
	// 我的群组列表
	groupList: function(param) {
		return http.post('/app/group/list', param);
	},
	// 创建群组
	createGroup: function(param) {
		return http.post('app/group/create', param);
	},
	transferGroup: function(param) {
		return http.post('/app/group/transferGroup', param);
	},
	// 删除退出
	groupDel: function(param) {
		return http.post('/app/group/msg/clearGroupMsg', param);
	},
	// 禁止发言
	groupDis: function(param) {
		return http.post('/app/group/dis', param);
	},
	// 清空群消息
	clearGroupMsg: function(param) {
		return http.post('/app/group/msg/clearGroupMsg', param);
	},
	addGroupAsk: function(param) {
		return http.post('/app/group/ask/add', param);
	},
	acceptGroup: function(param) {
		return http.post('/app/group/ask/accept', param);
	},
	refuseGroup: function(param) {
		return http.post('/app/group/ask/refuse', param);
	}
};
