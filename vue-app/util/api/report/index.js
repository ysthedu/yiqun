import http from '@/util/requestConfig.js';

export default {
	addReport: function(param) {
		return http.post('/app/report/add', param);
	}
};
