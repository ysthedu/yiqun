import http from '@/util/requestConfig'
import store from '@/store/index.js'
import friend from '@/util/api/friend'
import group from '@/util/api/group'
import post from '@/util/api/post'
import chat from '@/util/api/chat'
import conversation from '@/util/api/conversation'
import { putGroupMsg, putFriendMsg, addChatAll } from '@/util/db/db.js'
const api = {
	init: function(userId){
		return new Promise((resolve, reject) => {
			let reqGroup = {
				pageNum: 1,
				pageSize: 1000,
				userId
			}
			post.postList( {userId}).then(res=>{
				store.commit("setPost", res)
			});
			friend.friendList({userId}).then(res=>{
				for(var i in res){
					for(var j in res[i].members){
						if(res[i].members.length>0){
							let chatId = res[i].members[j].id;
							let reqMsg = {
								userId,
								chatType: 0,
								chatId,
								pageNum: 1,
								pageSize: 10
							}
							chat.historyMsgList(reqMsg).then(resMsg=>{
								putFriendMsg(chatId,resMsg)
							});
						}
					}
				}
				store.commit("setFriend", res)
			});
			group.groupList( reqGroup).then(res=>{
				store.commit("setGroup", res.data)
			});
			
			chat.queryLastMsg({}).then(a=>{
				putGroupMsg(a)
			});
			
			conversation.conversationList({}).then(res=>{
				addChatAll(res)
				store.commit("setConversation",res)
			});
			
			chat.getGroupIds({ userId }).then(res1=>{
				store.commit("setGroupIds", res1);
			});
			
		});
	}
}
export default api