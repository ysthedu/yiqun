import http from '@/util/requestConfig.js';

export default {
	getGroupIds:function(params){
		return http.post('/app/group/user/queryGroupIds', params)
	},
	historyMsgList:function(params){
		return http.post('/app/msg/list', params);
	},
	// 发起群聊
	appGroupCreate: function(params) {
		return http.post('/app/group/create', params);
	},
	//群详情、群成员
	getGroupMember: function(params) {
		return http.post('/app/group/member', params);
	},
	//更新群名称
	upGroupName: function(params) {
		return http.post('/app/group/upGroupName', params);
	},
	//更新群自己的群昵称
	upGroupNickName: function(params) {
		return http.post('/app/group/user/upGroupNickName', params);
	},
	//增加成员
	groupUserAdd: function(params) {
		return http.post('/app/group/user/add', params);
	},
	//移除成员
	groupUserDel: function(params) {
		return http.post('/app/group/user/del', params);
	},
	//转让群主
	msgAdd: function (param) {
		return http.post('/app/msg/add', param);
	},
	queryLastMsg: function (param){
		return http.post('/app/group/msg/queryLastMsg', param)
	}
}
