import http from '@/util/requestConfig.js';

export default {

	//创建会话
	createConversation : function  (param) {
		return http.post('/app/conversation/create',param);
	},
	
	//删除会话
	delConversation : function  (param) {
		return http.post('/app/conversation/del',param);
	},
	
	//会话列表
	conversationList : function  (param) {
		return http.post('/app/conversation/list',param);
	},
	
	//打开会话
	openConversation : function  (param) {
		return http.post('/app/conversation/open',param);
	},
	
	//更新会话
	updateConversation : function  (param) {
		return http.post('/app/conversation/update',param);
	},
	
};