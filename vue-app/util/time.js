const Time = {
	watchTimerInterval: null,
	countDownInterval: null,
	// 格式化时间
	formatTime: (number) => {
		let date = new Date(time * 1000);
		let year = date.getFullYear();
		let month = date.getMonth() + 1;
		let day = date.getDate();
		let hour = date.getHours();
		let minute = date.getMinutes();
		let second = date.getSeconds();
		return year + "-" + month.toString().padStart(2, "0") + "-" + day.toString().padStart(2, "0") +
			" " + hour.toString().padStart(2, "0") + ":" + minute.toString().padStart(2, "0") +
			":" + second.toString().padStart(2, "0");
	},

	watchTimer: (func) => {
		Time.watchTimerClear() // 清除老的
		let hour, minute, second; /*时 分 秒*/
		let hourStr, minuteStr, secondStr; /*时 分 秒*/
		hour = minute = second = 0; //初始化
		hourStr = minuteStr = secondStr = '';
		Time.watchTimerInterval = setInterval(() => {
			second += 1
			if (second >= 60) {
				second = 0;
				minute = minute + 1;
			}

			if (second < 10) {
				secondStr = '0' + second;
			} else {
				secondStr = '' + second;
			}

			if (minute < 10) {
				minuteStr = '0' + minute
			} else {
				minuteStr = '' + minute
			}

			if (minute >= 60) {
				minute = 0;
				hour = hour + 1;
			}

			if (hour < 10) {
				hourStr = '0' + hour
			} else {
				hourStr = '' + hour
			}
			if (typeof func == "function") {
				func({
					str: {
						hourStr: hourStr,
						minuteStr: minuteStr,
						secondStr: secondStr,
					},
					int: {
						hour: hour,
						minute: minute,
						second: second,
					}
				})
			}
		}, 1000);
	},

	watchTimerClear: () => {
		if (Time.watchTimerInterval) {
			clearInterval(Time.watchTimerInterval)
		}
	},

	countDown: (timer, func, endFunc) => {
		Time.countDownClerar();
		let timerNum = parseInt(timer)
		if (isNaN(timerNum) || timer <= 0) return timer;
		let hour, minute, second; /*时 分 秒*/
		hour = minute = second = 0; //初始化
		Time.countDownInterval = setInterval(() => {

			if (timerNum == 0) {
				Time.countDownClerar();
				if (typeof endFunc == "function") {
					endFunc();
				}
			}

			if (typeof func == "function") {
				hour = Math.floor(timerNum / 3600);
				minute = Math.floor((timerNum - hour * 3600) / 60);
				second = (timerNum % 60)
				let res = {
					str: {
						hourStr: hour < 10 ? '0' + hour : '' + hour,
						minuteStr: minute < 10 ? '0' + minute : '' + minute,
						secondStr: second < 10 ? '0' + second : '' + second,
					},
					int: {
						hour: hour,
						minute: minute,
						second: second,
					},
				}
				func(res)
			}

			timerNum -= 1

		}, 1000);
	},

	countDownClerar: () => {
		if (Time.countDownInterval) {
			clearInterval(Time.countDownInterval)
		}
	},

	chatHomeTimeFormat: (timestamp) => {

		const oldTimestampObj = new Date(timestamp);

		const varTmp = new Date(oldTimestampObj);
		
		const oldTimestampTime = varTmp.getTime();
		
		// 今天凌晨时间戳
		const beforeDawnTimestamp = new Date().setHours(0, 0, 0, 0)

		// 上周日凌晨时间戳
		const lastSundayBeforeDawnTimestamp = new Date().setHours(((new Date(beforeDawnTimestamp)).getDay() -
			1) * -24, 0, 0, 0)

		const varTmpFullYear = varTmp.getFullYear()
		const varTmpMonth = varTmp.getMonth()
		const varTmpDate = varTmp.getDate()
		const varTmpHours = varTmp.getHours()
		const varTmpMinutes = varTmp.getMinutes()
		const varTmpSeconds = varTmp.getSeconds()
		const varTmpWeekDay = varTmp.getDay()

		const weekArr = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"]

		let minutesAttr = ""
		if (varTmpMinutes >= 10) {
			minutesAttr += varTmpMinutes
		} else {
			minutesAttr += ("0" + varTmpMinutes)
		}

		const retChatTimeFormat = {
			hourAttr: "",
			weekAttr: "",
			yearAttr: ""
		}

		let hourAttr = ""

		if (varTmpHours < 12) {
			retChatTimeFormat.hourAttr = "上午 " + varTmpHours + ":" + minutesAttr
		}

		if (varTmpHours == 12) {
			retChatTimeFormat.hourAttr = "中午 " + varTmpHours + ":" + minutesAttr
		}

		if (varTmpHours > 12) {
			retChatTimeFormat.hourAttr = "下午 " + (varTmpHours - 12) + ":" + minutesAttr
		}

		if (oldTimestampTime > beforeDawnTimestamp) return retChatTimeFormat

		const yesterdayBeforeDawnTimestamp = new Date().setHours(-24, 0, 0, 0) // 今天凌晨时间戳

		if (oldTimestampTime > yesterdayBeforeDawnTimestamp) {

			retChatTimeFormat.hourAttr = "昨天 " + retChatTimeFormat.hourAttr

			return retChatTimeFormat
		}

		if (oldTimestampTime > lastSundayBeforeDawnTimestamp) {
			retChatTimeFormat.weekAttr = weekArr[varTmpWeekDay]
			return retChatTimeFormat
		}
		
		retChatTimeFormat.yearAttr = varTmpFullYear + "年" + varTmpMonth + "月" + varTmpDate + "日"

		return retChatTimeFormat
	}
}

export default Time;
