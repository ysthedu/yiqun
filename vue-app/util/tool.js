import courtConfig from "@/util/baseUrl.js"
import http from '@/util/requestConfig.js';
import store from '@/store/index.js'
import webim from '@/util/socket/webim.js'
import { addChat } from '@/util/db/db.js'
import friendReq from '@/util/api/friend/'
/**
 * 手机号验证
 */
export const phoneFun = (phone) => {
	let myreg = /^[1][3,4,5,7,8,9][0-9]{9}$/;
	if (!myreg.test(phone)) {
		return false;
	}
	return true;
}

//根据银行卡号获取银行图标
const bankParam = {
	bankNum:"",
	success:""
}

export const playMusic = () => {
	const innerAudioContext = uni.createInnerAudioContext();
	innerAudioContext.src = '/static/music/dingdong.mp3';
	innerAudioContext.play();
	plus.device.vibrate(1000);
}


export const getBankImg = (bankParam) => {
	let type = "";
	getBankCardInfo(bankParam)
}

//根据银行卡号获取银行类型
export const getBankCardInfo = (bankParam) => {
	let url = "https://ccdcapi.alipay.com/validateAndCacheCardInfo.json?_input_charset=utf-8&cardNo=" + bankParam.bankNum + "&cardBinCheck=true";
	uni.request({
		url: url,
		method: "GET",
		success: function(res) {
			if (res.statusCode === 200) {
				bankParam.success(res.data)
			}
		},
		fail: function(err) {
			// console.log("根据银行卡号获取银行类型错误-----", err)
		}
	})
}
// 通知群组
export const NoticeGroup = (packet) => {
	playMusic()
}
// 通知好友
export const NoticeFriend = (packet) => {
	if(packet.fromUserId != store.state.user.operId){
		store.commit("setNewFiend",{
			show: true,
			num: 1
		})
		playMusic()
	}
}


export const NoticeSucess = (packet) =>{
	friendReq.friendList({userId : store.state.user.operId}).then(res=>{
		store.commit("setFriend", res)
	});
}

export const NoticeMember = (packet) =>{
	const group = JSON.parse(packet.content);
	packet.toUserId = group.id
	packet.toUserName = group.groupName
	packet.toUserHeadImg = group.avatar
	packet.chatType = 1
	webim.joinGroup(group.id, res=>{
		addChat(packet)
	})
}